package com.kshrd.ams.repository.category;

import java.util.List;

import com.kshrd.ams.model.Category;


public interface CategoryRepository {
 List<Category> findAll();
 Category findOne(int id);
}
