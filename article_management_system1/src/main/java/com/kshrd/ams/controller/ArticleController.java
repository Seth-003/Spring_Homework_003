package com.kshrd.ams.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
//import java.nio.file.Files;
//import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.kshrd.ams.model.Article;
//import com.kshrd.ams.model.Category;
import com.kshrd.ams.service.ArticleService;
import com.kshrd.ams.service.category.CategoryService;

@Controller
public class ArticleController {
    
	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/article")
	public String article(ModelMap m) {
		List<Article> articles = articleService.findAll();
		m.addAttribute("articles",articles);
		return("article");
	}
	@GetMapping("/add")
	public String add(ModelMap m, Article article ) {
        m.addAttribute("artilce", article);
//        m.addAttribute("categories", categoryService.findAll());
        m.addAttribute("formAdd", true);
		return("add");
	}
	
	@PostMapping("/add")
	public String saveArticle(@RequestParam("image") MultipartFile thumbnail, @Valid @ModelAttribute 
			Article article,BindingResult result, ModelMap m, String serverPath) {
//		if(result.hasErrors()) {
//			article.setCategory(categoryService.findOne(article.getCategory().getId()));
//			m.addAttribute("article", article);
//	        m.addAttribute("formAdd", true);
//			return "add";
//		}
		
		
//		if (thumbnail.isEmpty()) {
//			return "add";
//		}
//		else 
//		{
//			try {
//                Files.copy(thumbnail.getInputStream(), Paths.get(serverPath,thumbnail));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//		}
//		
		
		article.setThumbnail("/image/" + thumbnail.getOriginalFilename());
		
		article.setCreateDate(new Date().toString());
		articleService.add(article);

		System.out.println(article);
		return "redirect:/add";
	}


	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
	@GetMapping("/update/{id}")
	public String update(@PathVariable int id,ModelMap m ) {
        m.addAttribute("artilce", articleService.findOne(id));
        
		return("update");
	}
	
	@PostMapping("/update/commit")
	public String saveUpdate(@ModelAttribute Article article) {
		articleService.update(article);
		return "redirect:/article";
	}
}	

