package com.kshrd.ams.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kshrd.ams.model.Category;


@Service
public class CategoryServiceImpl implements CategoryService{
	
    @Autowired
	private CategoryService categoryService;
//	@Autowired
//	public CategoryServiceImpl(CategoryService cateogryService) {
//		
//		this.categoryService = categoryService;
//		
//	}
	
	
	@Override
	public List<Category> findAll() {
		return categoryService.findAll();
	}

	@Override
	public Category findOne(int id) {
		
		return categoryService.findOne(id);
	}
 
}
