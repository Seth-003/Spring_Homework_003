package com.kshrd.ams.model;

import java.util.Locale.Category;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Article {
//   @NotNull
   private int id;
//   @NotBlank
   private String title;
   
   private com.kshrd.ams.model.Category category;
   
//   @NotBlank
   private String description;
   
   private String thumbnail;
   
//   @NotBlank
//   @Size(min=5, max=10)
   private String author;
   private String createDate;
public int getId() {
	return id;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public com.kshrd.ams.model.Category getCategory() {
	return category;
}

public void setCategory(com.kshrd.ams.model.Category category) {
	this.category = category;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public String getThumbnail() {
	return thumbnail;
}

public void setThumbnail(String thumbnail) {
	this.thumbnail = thumbnail;
}

public String getAuthor() {
	return author;
}

public void setAuthor(String author) {
	this.author = author;
}

public String getCreateDate() {
	return createDate;
}

public void setCreateDate(String createDate) {
	this.createDate = createDate;
}

public void setId(int id) {
	this.id = id;
}
public Article() {
	
}
public Article(int id, String title, com.kshrd.ams.model.Category category, String description, String thumbnail, String author,
		String createDate) {
	super();
	this.id = id;
	this.title = title;
	this.category = category;
	this.description = description;
	this.thumbnail = thumbnail;
	this.author = author;
	this.createDate = createDate;
}

@Override
public String toString() {
	return "Article [id=" + id + ", title=" + title + ", category=" + category + ", description=" + description
			+ ", thumbnail=" + thumbnail + ", author=" + author + ", createDate=" + createDate + "]";
}

   
}
