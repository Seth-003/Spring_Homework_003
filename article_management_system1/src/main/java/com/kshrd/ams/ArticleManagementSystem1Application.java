package com.kshrd.ams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleManagementSystem1Application {

	public static void main(String[] args) {
		SpringApplication.run(ArticleManagementSystem1Application.class, args);
	}
}
